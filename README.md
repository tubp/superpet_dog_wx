### 狗年最火的遛狗小程序，关联微信运动步数，用步数来遛狗。

微信小程序搜索 **“汪牌”** 进行体验。

这是小程序前端，后端请点击这里：[好玩的微信狗狗宠物——服务端](https://gitee.com/cqrun/superpet_dog_service)

 **Getting Start:** 

1. 申请一个小程序AppId（个人类的即可，企业类的更佳），下载微信开发者工具，将前端项目导入开发者工具，
2. 把AppId换成你的AppId，在globalConstant.js文件中配置你的后台地址，本地调试可以用127.0.0.1，发布到外网一定要用https的域名地址（没有https服务器，看这里配置[https服务器配置](http://itbang.me/solu/detail/96)）
3. 后台服务采用的是极速开发框架JFinal+MySQL，找到app_config.txt配置数据库，根目录下的sql目录即为数据结构和初始化数据
4. 不熟悉使用JFinal的话可以看这里[JFinal官网](http://www.jfinal.com/)

 **小程序截图：** 
![输入图片说明](https://gitee.com/uploads/images/2018/0329/223336_4bd1aec3_578271.jpeg "1.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223356_e39658c7_578271.jpeg "2.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223404_40177a98_578271.jpeg "3.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223411_62d4438b_578271.jpeg "4.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223423_a320c405_578271.jpeg "5.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223433_7f387e20_578271.jpeg "6.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223441_e0da3b81_578271.jpeg "7.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223452_6166a65a_578271.jpeg "8.jpg")

![输入图片说明](https://gitee.com/uploads/images/2018/0329/223503_c70522f2_578271.jpeg "9.jpg")

合作交流QQ：65405379
